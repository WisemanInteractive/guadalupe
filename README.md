# Guadalupe

A custom web UI theme for Fractal, based on Mandelbrot and Nighthawk.

See the Fractal [documentation](http://fractal.build/guide) for details on configuration and usage.
